const express = require('express');
const app = express();
const listingRoutes = require('./api/routes/listings');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/real_estate', { useNewUrlParser: true });

// handle CORS restrictions
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    if(req.method == 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
        return res.status(200).json({});
    }
    next();
});

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/listings', listingRoutes);

// handle any errors for routes which don't exist
app.use(function(req, res, next){
    const error = new Error('Not Found');
    error.status =404;
    next(error);
});

app.use(function(error, req, res, next){
    res.status(error.status || 500);
    res.json({error:{message: error.message}});
});

module.exports = app;