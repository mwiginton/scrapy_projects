const mongoose = require('mongoose');

const listingSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    address: String,
    price: Number
});

module.exports = mongoose.model('Listing', listingSchema, 'listings');