const express = require('express');
const router = express.Router();
const Listing = require('../models/listing');
const mongoose = require('mongoose');

// get all listings
router.get("/", (req, res, next) => {
    Listing.find()
      .exec()
      .then(docs => {
        console.log(docs);
        res.status(200).json(docs);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

// get listing by id
router.get("/:listingId", (req, res, next) => {
    const id = req.params.listingId;
    Listing.findById(id)
      .exec()
      .then(doc => {
        console.log("From database", doc);
        if (doc) {
          res.status(200).json(doc);
        } else {
          res
            .status(404)
            .json({ message: "No valid entry found for provided ID" });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
      });
  });
module.exports = router;