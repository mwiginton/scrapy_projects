# -*- coding: utf-8 -*-
import scrapy

class TruliaSpiderSpider(scrapy.Spider):
    name = 'trulia_spider'
    allowed_domains = ['trulia.com']
    start_urls = ['https://www.trulia.com/for_sale/77019_zip/2p_beds/0-350000_price/']

    def parse(self, response):

        listings = response.xpath('//li[@class="xsCol12Landscape smlCol12 lrgCol8"]')
        for listing in listings:
            # extract key listing info
            address = listing.xpath('.//div[@class="h6 typeWeightNormal typeTruncate typeLowlight mvn"]/text()').extract_first()
            price = listing.xpath('.//*[@class="cardPrice h5 man pan typeEmphasize noWrap typeTruncate"]/text()').extract_first()
            numBeds = listing.xpath('.//*[@data-testid="beds"]/text()').extract_first()
            numBaths = listing.xpath('.//*[@data-testid="baths"]/text()').extract_first()
            sqFt = listing.xpath('.//*[@data-testid="sqft"]/text()').extract_first()
            neighborhood = listing.xpath('.//div[@class="typeTruncate typeLowlight"]/text()').extract_first()
            img = listing.xpath('.//*[@class="tileLink"]/@href').extract_first()

            # convert price to integer
            updated_price = price.replace("$", "")
            updated_price = updated_price.replace(",", "")
            int_price = int(updated_price)

            # convert numBeds to integer
            updated_numBeds = numBeds.replace("bd", "")
            int_numBeds = int(updated_numBeds)

            # convert numBaths to integer
            updated_numBaths = numBaths.replace("ba", "")
            int_numBaths = int(updated_numBaths)

            # convert sqft to integer
            updated_sqFt = sqFt.replace(" sqft", "")
            updated_sqFt = updated_sqFt.replace(",", "")
            int_sqFt = int(updated_sqFt)

            yield{'address': address, 'price': int_price, 'numBeds': int_numBeds, 'numBaths': int_numBaths, 'sqFt': int_sqFt, 'neighborhood': neighborhood, 'img': 'https://www.trulia.com' + img, 'dataSource': 'trulia'}

        