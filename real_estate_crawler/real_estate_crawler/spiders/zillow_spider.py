# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector


class ZillowSpiderSpider(scrapy.Spider):
    name = 'zillow_spider'
    allowed_domains = ['zillow.com/homes/for_sale/77019_rb/']
    start_urls = ['https://www.zillow.com/homes/for_sale/77019_rb/']

    def parse(self, response):
        title = response.xpath("//div[@class='zsg-photo-card-content']").extract()

        yield {'title' : title}
        
