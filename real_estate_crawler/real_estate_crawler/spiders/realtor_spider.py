# -*- coding: utf-8 -*-
import scrapy


class RealtorSpiderSpider(scrapy.Spider):
    name = 'realtor_spider'
    allowed_domains = ['realtor.com']
    start_urls = ['https://www.realtor.com/realestateandhomes-search/77019/beds-2/price-na-350000']

    def parse(self, response):

        listings = response.xpath('//li[@class="component_property-card js-component_property-card js-quick-view"]')
        for listing in listings:
            address = listing.xpath('.//*[@class="listing-street-address"]/text()').extract_first()
            zipCode = listing.xpath('.//*[@class="listing-postal"]/text()').extract_first()
            price = listing.xpath('.//*[@class="data-price"]/text()').extract_first()
            numBeds = listing.xpath('.//*[@class="data-value meta-beds"]/text()').extract_first()

            # retrieve array that contains baths and sq ft info, then separete them 
            # into numBeds and numBaths
            bathsAndSqFt = listing.xpath('.//*[@class="data-value"]/text()').extract()
            numBaths = bathsAndSqFt[0]
            sqFt = bathsAndSqFt[1]

            imgThumbnail = listing.xpath('.//*[@class="photo-wrap "]').extract()
            images = listing.xpath('.//a[@class="photo-wrap "]').extract()


            yield{'address': address, 'zipCode': zipCode, 'price': price, 'numBeds': numBeds, 'numBaths': numBaths, 'sqFt': sqFt, 'dataSource': 'realtor.com'}
